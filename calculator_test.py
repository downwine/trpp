#from calculator import add, subtract, multiplication, division 
#firstNumber = 10 
#secondNumber = 5 
#def test_add(): 
#    assert add(firstNumber, secondNumber) == 15 
#def test_subtract(): 
#    assert subtract(firstNumber, secondNumber) == 5 
#def test_multiplication(): 
#    assert multiplication(firstNumber, secondNumber) == 50 
#def test_division(): 
#    assert division(firstNumber, secondNumber) == 2

from calculator import im_counting


def test_1():
    assert im_counting(12.2, "c", "F") == "12.2C = 53.96F"


def test_2():
    assert im_counting("fdcfdv", "K", "c") == "wrong input"


def test_3():
    assert im_counting(10, "c", "C") == "10.0C = 10.0C"


def test_4():
    assert im_counting(15, "erfrwjkfhbrw", "K") == "wrong input"


def test_5():
    assert im_counting(15, "F", "ehkbehkbfr") == "wrong input"


def test_6():
    assert im_counting(15, 4, "C") == "wrong input"


def test_7():
    assert im_counting(15, "f", 8) == "wrong input"


def test_8():
    assert im_counting(14, 17.5, "K") == "wrong input"


def test_9():
    assert im_counting(14, "c", 5.6) == "wrong input"


def test_10():
    assert im_counting(7, "r", 7) == "wrong input"


def test_11():
    assert im_counting(8, 9, "y") == "wrong input"


def test_12():
    assert im_counting(4, "w", "o") == "wrong input"


def test_13():
    assert im_counting(7, "K", "k") == "7.0K = 7.0K"


def test_14():
    assert im_counting(13, "C", "c") == "13.0C = 13.0C"


def test_15():
    assert im_counting(15, "c", "c") == "15.0C = 15.0C"


def test_16():
    assert im_counting(16, "C", "C") == "16.0C = 16.0C"


def test_17():
    assert im_counting(17, "k", "K") == "17.0K = 17.0K"


def test_18():
    assert im_counting(18, "K", "K") == "18.0K = 18.0K"


def test_19():
    assert im_counting(19, "k", "k") == "19.0K = 19.0K"


def test_20():
    assert im_counting(20, "f", "f") == "20.0F = 20.0F"


def test_21():
    assert im_counting(21, "F", "F") == "21.0F = 21.0F"


def test_22():
    assert im_counting(22, "f", "F") == "22.0F = 22.0F"


def test_23():
    assert im_counting(23, "F", "f") == "23.0F = 23.0F"


def test_24():
    assert im_counting(10, "c", "f") == "10.0C = 50.0F"


def test_25():
    assert im_counting(33, "f", "C") == "33.0F = 0.56C"


def test_26():
    assert im_counting(12.2, "f", "c") == "12.2F = -11.0C"


def test_27():
    assert im_counting(28, "f", "k") == "28.0F = 270.93K"


def test_28():
    assert im_counting(28, "K", "F") == "28.0K = -409.27F"


def test_29():
    assert im_counting(28.13, "F", "k") == "28.13F = 271.0K"


def test_30():
    assert im_counting(30.5, "K", "f") == "30.5K = -404.77F"


def test_31():
    assert im_counting(32, "K", "c") == "32.0K = -241.15C"


def test_32():
    assert im_counting(32, "C", "k") == "32.0C = 305.15K"


def test_33():
    assert im_counting(33.15, "k", "C") == "33.15K = -240.0C"


def test_34():
    assert im_counting(-240.15, "c", "K") == "-240.15C = 33.0K"

def im_counting(digit, start_scale, end_scale):
    f = open("tests.txt", "a")
    wr = "wrong input\n"

    try:
        float(digit)
    except ValueError:
        f.write(wr)
        f.close()
        return "wrong input"

    if not isinstance(start_scale, str) or not isinstance(end_scale, str):
        f.write(wr)
        f.close()
        return "wrong input"

    if start_scale != "C" and start_scale != "F" and start_scale != "K" and start_scale != "c" \
            and start_scale != "f" and start_scale != "k":
        f.write(wr)
        f.close()
        return "wrong input"

    if end_scale != "C" and end_scale != "F" and end_scale != "K" and end_scale != "c" \
            and end_scale != "f" and end_scale != "k":
        f.write(wr)
        f.close()
        return "wrong input"

    digit = float(digit)
    if start_scale == "c" or start_scale == "C":
        if end_scale == "c" or end_scale == "C":
            s = str(digit) + "C = " + str(digit) + "C"
            f.write(s + "\n")
            f.close()
            return s
        if end_scale == "f" or end_scale == "F":
            result = round(((9.0 * digit) / 5.0 + 32.0), 2)
            s = str(digit) + "C = " + str(result) + "F"
            f.write(s + "\n")
            f.close()
            return s
        if end_scale == "k" or end_scale == "K":
            result = round((digit + 273.15), 2)
            s = str(digit) + "C = " + str(result) + "K"
            f.write(s + "\n")
            f.close()
            return s
    if start_scale == "f" or start_scale == "F":
        if end_scale == "f" or end_scale == "F":
            s = str(digit) + "F = " + str(digit) + "F"
            f.write(s + "\n")
            f.close()
            return s
        if end_scale == "c" or end_scale == "C":
            result = round(((5.0 * (digit - 32.0)) / 9.0), 2)
            s = str(digit) + "F = " + str(result) + "C"
            f.write(s + "\n")
            f.close()
            return s
        if end_scale == "k" or end_scale == "K":
            result = round((((5.0 * (digit - 32.0)) / 9.0) + 273.15), 2)
            s = str(digit) + "F = " + str(result) + "K"
            f.write(s + "\n")
            f.close()
            return s
    if start_scale == "k" or start_scale == "K":
        if end_scale == "k" or end_scale == "K":
            s = str(digit) + "K = " + str(digit) + "K"
            f.write(s + "\n")
            f.close()
            return s
        if end_scale == "c" or end_scale == "C":
            result = round((digit - 273.15), 2)
            s = str(digit) + "K = " + str(result) + "C"
            f.write(s + "\n")
            f.close()
            return s
        if end_scale == "f" or end_scale == "F":
            result = round(((9.0 * (digit - 273.15)) / 5.0 + 32.0), 2)
            s = str(digit) + "K = " + str(result) + "F"
            f.write(s + "\n")
            f.close()
            return s


def __init__():
    if __name__ == '__main__':
        print("Введите число градусов: ")
        d = input()
        print("Введите начальную шкалу: ")
        ss = input()
        print("Введите целевую шкалу: ")
        es = input()
        res = im_counting(d, ss, es)
        print(res)

